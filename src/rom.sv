//Copyright (c) 2022 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module rom #(
   parameter DATA_WIDTH = 32,
   parameter ADDR_WIDTH = 10,
   parameter [DATA_WIDTH-1:0] CONTENTS [2**ADDR_WIDTH],
   parameter USE_BREG = 1
)(
   input  uwire                  clk_i,
   input  uwire [ADDR_WIDTH-1:0] addr_i,
   output reg   [DATA_WIDTH-1:0] data_o //1 cycle latency without BREG, 2 with
);
   reg [DATA_WIDTH-1:0] mem [2**ADDR_WIDTH] = CONTENTS;
   reg [DATA_WIDTH-1:0] dout;
   always @ (posedge clk_i) dout <= mem[addr_i];
   if (USE_BREG) always @ (posedge clk_i) data_o <= dout;
   else assign data_o = dout;
endmodule
`default_nettype wire
