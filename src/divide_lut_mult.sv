//Copyright (c) 2022 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module divide_lut_mult #(
   parameter N_WIDTH = 32,
   parameter D_WIDTH = 10,
   parameter PRECISION = D_WIDTH //Extra bits in inverse/multiplier. result is always >= ideal. D_WIDTH has no error. 0 has up to +1 error, negative numbers have a higher maximum error (but smaller multiplier)
)(
   input  uwire               clk_i  ,
   input  uwire [N_WIDTH-1:0] numer_i,
   input  uwire [D_WIDTH-1:0] deno_m1_i, //reduced by 1 to encode 1:2**D_WIDTH in D_WIDTH bits
   output reg   [N_WIDTH-1:0] div_o      //5 cycle pipeline delay from inputs
);

   localparam EXTRA_BITS = 0;
   reg [N_WIDTH-1            :0] numer_d   [1:3]; //2 to line up with ROM, and 1 more for DSP input register
   reg [N_WIDTH+PRECISION    :0] inverse_d [1:3];
   reg [N_WIDTH*2+PRECISION-1:0] mul; //DSP M reg

   typedef bit [N_WIDTH+PRECISION:0] t_rom_fill [2**D_WIDTH];
   function t_rom_fill F_ROM_FILL;
      t_rom_fill ret;
      for (int i=0; i<2**D_WIDTH; i++) ret[i] = {1'b0,{(N_WIDTH+PRECISION){1'b1}}}/(i+1)+1; //fixed point 0.N multiplicitive inverses of the denominator (ceiling)
      F_ROM_FILL = ret;
   endfunction
   localparam t_rom_fill MEM = F_ROM_FILL;

   always @ (posedge clk_i) begin
      //comments may not match up with actual inference, particularly when large enough to use DSP cascading
      numer_d[1] <= numer_i;    //fabric reg
      numer_d[2] <= numer_d[1]; //DSP ABREG[1]
      numer_d[3] <= numer_d[2]; //DSP ABREG[2]
      inverse_d[1] <= MEM[deno_m1_i]; //BRAM DOUT
      inverse_d[2] <= inverse_d[1]; //BRAM BREG
      inverse_d[3] <= inverse_d[2]; //DSP CREG
      mul <= numer_d[3]*inverse_d[3]; //DSP MREG
      div_o <= mul[N_WIDTH+PRECISION +: N_WIDTH]; //only keep the upper half, removing the fixed point offset. DSP PREG
   end
endmodule
`default_nettype wire
