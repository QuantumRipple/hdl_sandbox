//Copyright (c) 2022 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module gearbox #(
   parameter IN_WIDTH  = 10,
   parameter OUT_WIDTH = 32
)(
   input  uwire                  clk_i,
   input  uwire                  rst_i,
   input  uwire  [IN_WIDTH -1:0] dat_i,
   input  uwire                  dv_i ,
   output reg    [OUT_WIDTH-1:0] dat_o,
   output reg                    dv_o =0
);
   if (IN_WIDTH < OUT_WIDTH) begin //upsizing
      reg [OUT_WIDTH+IN_WIDTH-1:0] dat_internal;
      reg [$clog2(OUT_WIDTH)-1:0] pointer = 0;
      always @ (posedge clk_i) begin
         dv_o <= 0; //default
         if (dv_o) dat_internal[0+:IN_WIDTH] <= dat_internal[OUT_WIDTH+:IN_WIDTH]; //roll the overflow data to the start. Some bits may be extraneous, will be overridden by valid data if so.
         if (dv_i) begin
            dat_internal[pointer +: IN_WIDTH] <= dat_i;
            if (pointer+IN_WIDTH >= OUT_WIDTH) begin
               pointer <= pointer+IN_WIDTH-OUT_WIDTH;
               dv_o <= 1;
            end
            else pointer <= pointer+IN_WIDTH;
         end
         if (rst_i) begin
            pointer <= 0;
            dv_o <= 0;
         end
      end
      assign dat_o = dat_internal[0+:OUT_WIDTH];
   end
   else if (IN_WIDTH > OUT_WIDTH) begin //downsizing, has potential to lose data if input rate is too high/bursty
      reg [IN_WIDTH-1:0] dat_saved;
      reg [$clog2(IN_WIDTH+1)-1:0] pointer = IN_WIDTH;
      reg error = 0; //Data was lost due to new incoming data. not currently attached to anything.
      always @ (posedge clk_i) begin
         logic [$clog2(OUT_WIDTH)-1:0] valid_bits;
         dv_o <= 0; //default
         if (pointer+OUT_WIDTH <= IN_WIDTH) begin //enough data to play back a full output word
            pointer <= pointer+OUT_WIDTH;
            dat_o <= dat_saved[pointer +: OUT_WIDTH];
            dv_o <= 1;
            if (dv_i) error <= 1; //not done playing out previous input word
         end
         if (dv_i) begin
            dat_saved <= dat_i;
            if (pointer+OUT_WIDTH <= IN_WIDTH) valid_bits = 0;
            else                               valid_bits = {IN_WIDTH-pointer}; //concat operator to avoid truncation of operands. blocking for temporary use
            for (int i=0; i<OUT_WIDTH; i++) 
               if (i >= valid_bits) dat_o[i] <= dat_i[i-valid_bits]; //partial fill depends on how many bits were left over from the last input word
               else                 dat_o[i] <= dat_saved[pointer+i];
            dv_o <= 1;
            pointer <= OUT_WIDTH-valid_bits;
         end
         if (rst_i) begin
            pointer <= IN_WIDTH;
            dv_o <= 0;
         end
      end
   end
   else always @ (posedge clk_i) begin //no size change, single passthrough register
      dv_o  <= dv_i && !rst_i;
      dat_o <= dat_i;
   end
endmodule
`default_nettype wire
