//Copyright (c) 2022 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module tb_divide_lut_mult;
   localparam N = 16;//32; //don't go above 32 because of urandom call
   localparam D = 6;//10;
   localparam TEST_RAND_COUNT = 10_000_000;

   logic clk=0;
   always #5 clk = ~clk;

   reg   [D  :0] stim_d  ;
   reg   [N-1:0] stim_n  ;
   reg   [D  :0] d_delay  ;
   reg   [N-1:0] n_delay  ;
   uwire [D-1:0] stim_dm1;
   uwire [N-1:0] result  ;

   logic [31:0] n_vector [6] = '{32'hFFFF_FFFF, 32'hFFFF_FFFF, 32'hFFFF_FFFE, 32'h0000_00FF, 32'h0000_00FF, 32'h0000_00FF};
   logic [10:0] d_vector [6] = '{        10'd1,         10'd2,         10'd2,       10'h100,       10'h0FF,       10'h0FE};
   //localparam [31:0] r_vector [6] = '{32'hFFFF_FFFF, 32'h7FFF_FFFF, 32'h7FFF_FFFF, 32'h0000_0000, 32'h0000_0001, 32'h0000_0001};

   logic [N-1:0] r_expected,expected;

   assign stim_dm1 = stim_d - 1;
   divide_lut_mult #(
      .N_WIDTH  (N),
      .D_WIDTH  (D),
      .PRECISION(6)
   ) u_uut (
      .clk_i    (clk     ),
      .numer_i  (stim_n  ),
      .deno_m1_i(stim_dm1),
      .div_o    (result  )
   );


   initial begin
      $dumpfile("tb_divide_lut_mult.vcd");
      $dumpvars(0,tb_divide_lut_mult);
      @(posedge clk) #1;
      fork
         begin
            //for (int i=0; i<6; i++) begin
            //   @(posedge clk) #2;
            //   stim_n = n_vector[i];
            //   stim_d = d_vector[i];
            //end
            repeat (TEST_RAND_COUNT) begin
               @(posedge clk) #2;
               stim_n = $urandom;
               stim_d = $urandom_range(1,2**D);
               n_delay <= #48 stim_n;
               d_delay <= #48 stim_d;
               r_expected <= #48 stim_n/stim_d;
            end
         end
         begin
            repeat(5) @(posedge clk) #1; //align input and output
            //for (int j=0; j<6; j++) begin
            //   @(posedge clk) #1;
            //   expected = n_vector[j]/d_vector[j];
            //   assert (result == expected) else $error("got %0h instead of %1h for %2h / %3h", result, expected, n_vector[j], d_vector[j]);
            //end
            repeat (TEST_RAND_COUNT) begin
               @(posedge clk) #1;
               assert (result == r_expected  ) else $error("got %0h instead of %1h for random", result, r_expected);
               assert (result <= r_expected+1) else $error("got %0h instead of %1h for random (exceeds +1 error)", result, r_expected);
            end
         end
      join
      repeat(10) @(posedge clk) #1;
      $finish;
   end

endmodule
`default_nettype wire