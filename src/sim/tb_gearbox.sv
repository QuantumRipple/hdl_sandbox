//Copyright (c) 2022 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module tb_gearbox ();
   logic clk=0;
   always #5 clk = ~clk;

   logic rst=0;

   logic [ 9:0] dat_stim;
   logic        dv_stim=0;

   uwire [31:0] dat_upsized;
   uwire        dv_upsized;

   uwire [ 9:0] dat_downsized;
   uwire        dv_downsized;
   

   gearbox #(
      .IN_WIDTH (10),
      .OUT_WIDTH(32)
   ) u_uut_upsizer (
      .clk_i(clk         ),
      .rst_i(rst         ),
      .dat_i(dat_stim    ),
      .dv_i (dv_stim     ),
      .dat_o(dat_upsized ),
      .dv_o (dv_upsized  )
   );

   gearbox #(
      .IN_WIDTH (32),
      .OUT_WIDTH(10)
   ) u_uut_downsizer (
      .clk_i(clk           ),
      .rst_i(rst           ),
      .dat_i(dat_upsized   ),
      .dv_i (dv_upsized    ),
      .dat_o(dat_downsized ),
      .dv_o (dv_downsized  )
   );

   initial begin
      $dumpfile("tb_gearbox.vcd");
      $dumpvars(0,tb_gearbox);
      repeat(10) @(posedge clk) #1;
      dv_stim  = 1;
      dat_stim = 0;
      @(posedge clk) #1;
      dat_stim = 1;
      @(posedge clk) #1;
      dat_stim = 2;
      @(posedge clk) #1;
      dat_stim = 3;
      @(posedge clk) #1;
      dat_stim = 4;
      @(posedge clk) #1;
      dat_stim = 5;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 6;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 7;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 8;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 9;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 10;
      @(posedge clk) #1;
      //dv_stim = 0;
      dat_stim = 10'h3FF;
      @(posedge clk) #1;
      dv_stim = 1;
      dat_stim = 11;
      @(posedge clk) #1;
      dv_stim = 0;
      dat_stim = 10'h3FF;
      repeat(10) @(posedge clk) #1;
      $finish;
   end
endmodule
`default_nettype wire
